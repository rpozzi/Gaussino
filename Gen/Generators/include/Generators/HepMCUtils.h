/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: HepMCUtils.h,v 1.8 2008-07-23 17:21:55 cattanem Exp $
#ifndef GENERATORS_HEPMCUTILS_H
#define GENERATORS_HEPMCUTILS_H 1

// This include has been moved to Event/GenEvent package.
// This file is provided for backward compatibility.
#warning "You should now include GenEvent/HepMCUtils.h instead"
#include "GenEvent/HepMCUtils.h"

#endif // GENERATORS_HEPMCUTILS_H
