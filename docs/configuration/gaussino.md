# Gaussino

## Main configuration

```{eval-rst}
.. currentmodule:: Gaussino.Configuration

.. autoclass:: Gaussino
   :show-inheritance:
   :undoc-members:
   :special-members: __apply_configuration__
   :members:
   :private-members:
```
